import request from '../utils/request'
import qs from 'qs'

 //获取机构编号(已做权限)
 export function getOrgList(param) {
    return request({
      url:'/wxAppInfo/ry_getOrgByOrgsysnumAndOrgname',
      method:'post',
      data: qs.stringify(param, { indices: false, allowDots: true })
    })
 }
