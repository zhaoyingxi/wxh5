import request from '../utils/request'
import qs from 'qs'

// 获取应用列表数据
export function getList(param) {
  return request({
    url: '/wxAppInfo/ry_selectAllAppInfo',
    method: 'post',
    data: qs.stringify(param),
  })
}

// 获取应用类型select  eq:数据字典接口
export function getselectList(query) {
  return request({
    url: '/wxAppInfo/getAllSysparam',
    method: 'post',
    data: qs.stringify(query, { indices: false })
  })
}

//新增应用
export  function addInfo(param) {
  return request({
    url:'/wxAppInfo/ry_insertAppInfo',
    method:'post',
    data: qs.stringify(param, { indices: false , strictNullHandling: true }),
  })
}

//修改应用
export function editInfo(param) {
  return request({
    url:'/wxAppInfo/ry_updateAppInfo',
    method:'post',
    data: qs.stringify(param, {indices: false}),
  })
}
// 删除应用
export function deleteInfo(param) {
  return request({
    url: '/wxAppInfo/ry_deleteAppInfo',
    method: 'post',
    data: qs.stringify(param, {indices: false}),
  })
}

// 详情应用
export function ShowDetail(param) {
  return request({
    url:'/wxAppInfo/ry_selectByAppnum',
    method:'post',
    data: qs.stringify(param, {indices: false}),
  })
}

//素材数据
export function MaterialData(param) {
  return request({
    url:'/wxAppInfo/selectByMaterial',
    method:'post',
    data: qs.stringify(param, {indices: false})
  })

}

// 根据机构所输入关键字以及用户所在机构编号iOrgsysnum查询
export function getMaterialInfo(param) {
  return request({
    url:'/wxAppInfo/ry_getMaterialInfoByOrgsysnumAndOrgname',
    method:'post',
    data: qs.stringify(param, {indices: false})
  })
}

// 提交所选素材
export function submitMaterial(param) {
  return request({
    url:'/wxAppInfo/addRefAppList',
    method:'post',
    data: qs.stringify(param, { indices: false, allowDots: true })
  })
}

//移除素材
 export function scdeleteInfo(param) {
  return request({
    url:'/wxAppInfo/deleteRefApp',
    method:'post',
    data: qs.stringify(param, { indices: false, allowDots: true })
  })
 }

 //获取机构编号(已做权限)
 export function getOrgList(param) {
    return request({
      url:'/wxAppInfo/ry_getOrgByOrgsysnumAndOrgname',
      method:'post',
      data: qs.stringify(param, { indices: false, allowDots: true })
    })
 }
