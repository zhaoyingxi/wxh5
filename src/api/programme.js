import request from '../utils/request'
import qs from 'qs'

// 获取节目单列表数据
export function getList(param) {
  return request({
    url: '/WxprogramList/ry_getProgramList',
    method: 'post',
    data: qs.stringify(param),
  })
}

//（详情节目单）所关联的素材
export function getmaterialList(param) {
  return request({
    url: '/WxprogramList/ry_getMaterialInfoByProgramsysnum',
    method: 'post',
    data: qs.stringify(param)
  })
}

//删除节目单
export function deleteInfo(param) {
  return request({
    url: '/WxprogramList/ry_deleteProgramList',
    method: 'post',
    data: qs.stringify(param, {indices: false})
  })
}

//新增节目单
export function addprogramme(param) {
  return request({
    url: '/WxprogramList/ry_addProgramList',
    method: 'post',
    data: qs.stringify(param, {indices: false, allowDots: true})
  })
}

//新增节目单中获取素材
export function getPMaterialList(param) {
  return request({
    url: '/WxprogramList/ry_getMaterialInfoByOrgsysnum',
    method: 'post',
    data: qs.stringify(param)
  })
}

//获取设备列表
export function getdeviceOrg(param) {
  return request({
    url: '/WxprogramList/ry_getDeviceByOrgsysnum',
    method: 'post',
    data: qs.stringify(param)
  })
}
