import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)
/* Layout */
import Layout from '@/layout'

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/',
    name: 'Home',
    component: () => import('@/views/login/index'),
    // component: Layout,
    hidden: true,
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('@/views/login/index'),
    hidden: true,
  },
  {
    path: '/Menus',
    name: 'Menus',
    component: () => import('@/views/Menus/index'),
    hidden: true,
  },
  {
    path: '/test',
    component: Layout,
    children: [
      {
        path: 'index',
        name: 'test',
        meta: {
          title: '测试',
        },
        component: () => import('@/views/test/index'),
      },
    ],
    hidden: true,
  },
  {
    path: '/material',
    component: Layout,
    children: [
      {
        path: 'list',
        name: 'material',
        meta: {
          title: '素材列表',
        },
        component: () => import('@/views/material/index'),
      },
      {
        path: 'addMaterial',
        name: 'addMaterial',
        meta: {
          title: '新增素材',
        },
        component: () => import('@/views/material/addMaterial'),
      },
      {
        path: 'details',
        name: 'details',
        meta: {
          title: '素材详情',
        },
        component: () => import('@/views/material/details'),
      },
    ],
    hidden: true,
  },
  {
    path: '/application',
    component: Layout,
    children: [
      {
        path: 'list',
        name: 'application',
        meta: {
          title: '应用列表',
        },
        component: () => import('@/views/application/index'),
      },
      {
        path: 'addApplication',
        name: 'addApplication',
        meta: {
          title: '',
        },
        component: () => import('@/views/application/addApplication'),
      },
      {
        path: 'detailApplication',
        name: 'detailApplication',
        meta: {
          title: '应用详情',
        },
        component: () => import('@/views/application/detailApplication'),
      },
      {
        path: 'ApplicationData',
        name: 'ApplicationData',
        meta: {
          title: '应用数据',
        },
        component: () => import('@/views/application/ApplicationData'),
      },
      {
        path: 'MaterialData',
        name: 'MaterialData',
        meta: {
          title: '素材数据',
        },
        component: () => import('@/views/application/MaterialData'),
      },
      {
        path: 'addMaterialData',
        name: 'addMaterialData',
        meta: {
          title: '选择素材',
        },
        component: () => import('@/views/application/addMaterialData'),
      },
      {
        path: 'SearchOrg',
        name: 'SearchOrg',
        meta: {
          title: '选择机构'
        },
        component: () => import('@/views/application/SearchOrg')
      }
    ],
    hidden: true,
  },
  {
    path: '/programme',
    component: Layout,
    children: [
      {
        path: 'list',
        name: 'programme',
        meta: {
          title: '节目单列表',
        },
        component: () => import('@/views/programme/index'),
      },
      {
        path: 'detailProgramme',
        name: 'detailProgramme',
        meta: {
          title: '节目单详情'
        },
        component: () => import('@/views/programme/detailProgramme')
      },
      {
        path: 'MaterialList',
        name: 'MaterialList',
        meta: {
          title: '搜索出素材数据'
        },
        component: () => import('@/views/programme/MaterialList')
      },
      {
        path: 'addProgramme',
        name: 'addProgramme',
        meta: {
          title: ''
        },
        component: () => import('@/views/programme/addProgramme')
      }
    ],
    hidden: true,
  },
]

/**
 * asyncRoutes
 * the routes that need to be dynamically loaded based on user roles
 */
export const asyncRoutes = [
  // 404 page must be placed at the end !!!
  {path: '*', redirect: '/404', hidden: true},
]

const createRouter = () =>
  new Router({
    // mode: 'history', // require service support
    scrollBehavior: () => ({y: 0}),
    routes: constantRoutes,
  })

const router = createRouter()

export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
