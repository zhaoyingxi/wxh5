import request from '../utils/request'
import qs from 'qs'

export function addPieceData(param) {
  return request({
    url: '/material/addMaterialInfo',
    method: 'post',
    data: qs.stringify(param),
  })
}

export function modifieData(param) {
  return request({
    url: '/material/upMaterialInfo',
    method: 'post',
    data: qs.stringify(param),
  })
}

export function getList(param) {
  return request({
    url: '/material/getSelectTypeMaterInfo',
    method: 'post',
    data: qs.stringify(param),
  })
}

export function deletRow(idInfo) {
  return request({
    url: '/material/detMaterialInfo',
    method: 'post',
    data: qs.stringify(idInfo, { indices: false }),
  })
}

export function getOpts(param) {
  return request({
    url: '/system/getSystemType',
    method: 'post',
    data: qs.stringify(param, { indices: false }),
  })
}

export function fetchlist(param) {
  return request({
    url: '/material/getSelectTypeMaterInfo',
    method: 'post',
    data: qs.stringify(param, { indices: false }),
  })
}

export function fetchOrglist() {
  return request({
    url: '/device/getAllOrgInfo',
    method: 'post',
  })
}
