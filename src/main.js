import Vue from 'vue'
import './cube-ui'

import 'normalize.css/normalize.css' // a modern alternative to CSS resets

import 'amfe-flexible/index.js'
import '@/styles/index.scss' // global css

import App from './App.vue'
// import store from './store'
import router from './router'

Vue.config.productionTip = false

import Vant from 'vant'
import 'vant/lib/index.css'
import 'amfe-flexible'

Vue.use(Vant)

new Vue({
  router,
  // store,
  render: (h) => h(App),
}).$mount('#app')
