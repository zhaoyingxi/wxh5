import request from '@/utils/request'
import qs from 'qs'

/**
 * 登录
 * @param Object
 *
 */
export function login(query) {
  return request({
    url: '/wxAuth/wxlogin',
    method: 'post',
    data: qs.stringify(query),
  })
}
