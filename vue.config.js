const path = require('path')
const autoprefixer = require('autoprefixer')
const pxtorem = require('postcss-pxtorem')
function resolve(dir) {
  return path.join(__dirname, dir)
}
module.exports = {
  publicPath: '/',
  outputDir: 'dist',
  assetsDir: 'static',
  lintOnSave: false,
  productionSourceMap: false,

  devServer: {
    port: 3000,
    open: false,
    overlay: {
      warnings: false,
      errors: true,
    },
    disableHostCheck: true,
    proxy: {
      [process.env.VUE_APP_BASE_API]: {
        target: `http://47.114.78.203:8888/`, // 线上
        changeOrigin: true,
        pathRewrite: {
          ['^' + process.env.VUE_APP_BASE_API]: '',
        },
      },
    },
  },

  pages: {
    index: {
      // page 的入口
      entry: 'src/main.js',
      // 模板来源
      template: 'public/index.html',
      // 在 dist/index.html 的输出
      filename: 'index.html',
      // 当使用 title 选项时，
      // template 中的 title 标签需要是 <title><%= htmlWebpackPlugin.options.title %></title>
      title: 'TopSales 服务平台',
      // 在这个页面中包含的块，默认情况下会包含
      // 提取出来的通用 chunk 和 vendor chunk。
      // chunks: ['chunk-vendors', 'chunk-common', 'index'],
    },
  },

  configureWebpack: {
    name: 'TopSales 服务平台',
    resolve: {
      alias: {
        '@': resolve('src'),
      },
    },
  },

  css: {
    sourceMap: false,
    loaderOptions: {
      postcss: {
        plugins: [
          // function plugin(css, result) {
          //     var prefixes = loadPrefixes({
          //       from: css.source && css.source.input.file,
          //       env: options.env
          //     });
          //     timeCapsule(result, prefixes);
          
          //     if (options.remove !== false) {
          //       prefixes.processor.remove(css, result);
          //     }
          
          //     if (options.add !== false) {
          //       prefixes.processor.add(css, result);
          //     }
          //   },
          // css => {
          //     const exclude = opts.exclude;
          //     const filePath = css.source.input.file;
          //     if (
          //       exclude &&
          //       ((type.isFunction(exclude) && exclude(filePath)) ||
          //         (type.isString(exclude) && filePath.indexOf(exclude) !== -1) ||
          //         filePath.match(exclude) !== null)
          //     ) {
          //       return;
          //     }
          
          //     const rootValue =
          //       typeof opts.rootValue === "function"
          //         ? opts.rootValue(css.source.input)
          //         : opts.rootValue;
          //     const pxReplace = createPxReplace(
          //       rootValue,
          //       opts.unitPrecision,
          //       opts.minPixelValue
          //     );
          
          //     css.walkDecls((decl, i) => {
          //       if (
          //         decl.value.indexOf("px") === -1 ||
          //         !satisfyPropList(decl.prop) ||
          //         blacklistedSelector(opts.selectorBlackList, decl.parent.selector)
          //       )
          //         return;
          
          //       const value = decl.value.replace(pxRegex, pxReplace);
          
          //       // if rem unit already exists, do not add or replace
          //       if (declarationExists(decl.parent, decl.prop, value)) return;
          
          //       if (opts.replace) {
          //         decl.value = value;
          //       } else {
          //         decl.parent.insertAfter(i, decl.clone({ value: value }));
          //       }
          //     });
          
          //     if (opts.mediaQuery) {
          //       css.walkAtRules("media", rule => {
          //         if (rule.params.indexOf("px") === -1) return;
          //         rule.params = rule.params.replace(pxRegex, pxReplace);
          //       });
          //     }
          //   }
           autoprefixer(),
          pxtorem({
            rootValue: 37.5,
            propList: ['*'],
          }),
        ]
      },
      stylus: {
        'resolve url': true,
        'import': [
          './src/theme'
        ]
      }
    },
  },

  pluginOptions: {
    'cube-ui': {
      postCompile: true,
      theme: true
    }
  }
}
