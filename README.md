# wxh5

## 开发

```bash
# 克隆项目
git clone https://gitee.com/zhaoyingxi/wxh5.git

# 进入项目目录
cd wxh5

# 安装依赖
npm install

# 建议不要直接使用 cnpm 安装依赖，会有各种诡异的 bug。可以通过如下操作解决 npm 下载速度慢的问题
npm install --registry=https://registry.npm.taobao.org

# 启动服务
npm run serve

```

浏览器访问 http://localhost:3000

## 发布

```bash
# 构建
npm run build

```

## 其它

```bash
# 代码格式检查
npm run lint

# 代码格式检查并自动修复
npm run lint -- --fix

```
